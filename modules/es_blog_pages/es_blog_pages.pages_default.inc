<?php
/**
 * @file
 * es_blog_pages.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function es_blog_pages_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'magazine';
  $page->task = 'page';
  $page->admin_title = 'Magazin';
  $page->admin_description = '';
  $page->path = 'magazin';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Magazin',
    'weight' => '0',
    'name' => 'navigation',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
      'name' => 'navigation',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_magazine__panel_context_84645af3-8f40-42e8-b62f-485237d37b71';
  $handler->task = 'page';
  $handler->subtask = 'magazine';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => FALSE,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Magazine';
  $display->uuid = '0f3d68b0-5799-4835-800e-40f7af0958ef';
  $display->storage_type = '';
  $display->storage_id = '';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-839e9c63-4ba1-482f-8dcc-cb214d012c65';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'es_blog_recent_blog_posts';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '0',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'default',
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '839e9c63-4ba1-482f-8dcc-cb214d012c65';
  $display->content['new-839e9c63-4ba1-482f-8dcc-cb214d012c65'] = $pane;
  $display->panels['middle'][0] = 'new-839e9c63-4ba1-482f-8dcc-cb214d012c65';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['magazine'] = $page;

  return $pages;

}
